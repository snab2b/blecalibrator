import React from 'react'
import MainStack from './src/stacks/MainStack'
import { NavigationContainer } from '@react-navigation/native'
import { ConnectionsContextProvider } from './src/screens/bluetooth/context/ConnectionsContext'
import { MainContextProvider } from './src/context/main'

export default () => {
  return (
    <MainContextProvider>
      <ConnectionsContextProvider>
        <NavigationContainer>
          <MainStack />
        </NavigationContainer>
      </ConnectionsContextProvider>
    </MainContextProvider>
  );
};

