import React, { createContext, useState, useEffect } from 'react'

export const MainContext = createContext()

export const MainContextProvider = ({ children }) => {
  const [calibrationTable, setCalibrationTable] = useState(null)
  const [step, setStep] = useState(1)

  // useEffect(() => {
  //   if (calibrationTable) console.log('calibrationTable', calibrationTable)
  // }, [calibrationTable])

  return (
    <MainContext.Provider value={{
      calibrationTable, setCalibrationTable,
      step, setStep
    }}>
      {children}
    </MainContext.Provider>
  )
}
