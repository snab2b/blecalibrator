import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import CustomTopBar from '../components/CustomTopBar/CustomTopBar'
import Start from '../screens/Start'
import StartFake from '../screens/StartFake'
import Bluetooth from '../screens/Bluetooth'
import CabinetTab from './CabinetTab'
import CalibratorStack from './CalibratorStack'

const Stack = createStackNavigator()

export default () => {
  return (
    <Stack.Navigator
      initialRouteName="StartFake"
      screenOptions={{
        headerShown: false,
      }}>

      <Stack.Screen
        name="Start"
        component={Start} />

      <Stack.Screen
        name="StartFake"
        component={StartFake} />

      <Stack.Screen
        name="Bluetooth"
        component={Bluetooth}
        options={{
          header: props => <CustomTopBar {...props}
            label={['Поиск', 'устройства']}
            typeImage="back"
          />,
          headerShown: true,
        }}
      />

      <Stack.Screen
        name="CabinetTab"
        component={CabinetTab}
      />

    </Stack.Navigator>
  )
}
