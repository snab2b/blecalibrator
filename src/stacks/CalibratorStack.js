import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import CalibratorStep1 from '../screens/CalibratorStep1';
import CustomBottomBar from '../components/CustomBottomBar/CustomBottomBar';

const Tab = createBottomTabNavigator();

export default () => {
  return (
    <Tab.Navigator
      initialRouteName="CalibratorStep1"
      screenOptions={{
        headerShown: true,
      }}
      tabBar={props => <CustomBottomBar {...props} />}>

      <Tab.Screen
        name="CalibratorStep1"
        component={CalibratorStep1}/>

    </Tab.Navigator>
  );
}

