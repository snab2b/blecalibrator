import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import CustomBottomBar from '../components/CustomBottomBar/CustomBottomBar';
import Fuel from '../screens/Fuel';
import Test from '../screens/Test';
import State from '../screens/State';
import Contact from '../screens/Contact';
import CalibratorStep1 from '../screens/CalibratorStep1';
import CalibratorStep2 from '../screens/CalibratorStep2';
import CalibratorStep3 from '../screens/CalibratorStep3';
import FinishCalibration from '../screens/FinishCalibration';
import TableCalibration from '../screens/TableCalibration';

const Tab = createBottomTabNavigator();

const CabinetTab = () => {
  return (
      <Tab.Navigator
        initialRouteName="CalibratorStep1"
        screenOptions={{
          headerShown: true,
        }}
        tabBar={props => <CustomBottomBar {...props} />}>
        <Tab.Screen name="State" component={State}/>
        <Tab.Screen name="Fuel" component={Fuel}/>
        <Tab.Screen name="Contact" component={Contact}/>
        <Tab.Screen name="CalibratorStep1" component={CalibratorStep1}/>
        <Tab.Screen name="CalibratorStep2" component={CalibratorStep2}/>
        <Tab.Screen name="CalibratorStep3" component={CalibratorStep3}/>
        <Tab.Screen name="FinishCalibration" component={FinishCalibration}/>
        <Tab.Screen name="TableCalibration" component={TableCalibration}/>
        <Tab.Screen name="Test" component={Test}/>
      </Tab.Navigator>
  );
};

export default CabinetTab;
