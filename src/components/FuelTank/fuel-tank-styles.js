import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  FuelTankContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    position: 'relative'
    // borderColor: '#f00',
    // borderWidth: 1
  },
  FuelTankImage: {
    width: 167,
    height: 206,
    marginTop: 58
  },
  FuelTankJalousieContainer: {
    width: 167,
    height: 206,
    marginTop: 58,
    position: 'absolute',
    top: '50%',
    marginTop: -107
  },
  FuelTankJalousie: {
    width: '100%',
    height: '0%',
    backgroundColor: '#fff'
  },
  FuelTankBorder: {
    width: 192,
    height: 262,
    position: 'absolute',
    bottom: '50%',
    marginBottom: -115,
  },
  FuelRulerContainer: {
    position: 'absolute',
    height: 234,
    width: 43,
    bottom: '50%',
    marginBottom: -115,
    left: '50%',
    marginLeft: 105,
    borderColor: '#C5CBD9',
    borderWidth: 1,
    borderStyle: 'dashed',
    borderRadius: 1,
  },
  FuelRuler: {
    position: 'absolute',
    top: -1,
    left: -1,
    right: 0,
    bottom: -1,
    borderRadius: 1,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderLeftWidth: 2,
    borderLeftColor: '#fff',
    borderBottomColor: '#C5CBD9',
    borderTopColor: '#C5CBD9',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 16,
    paddingBottom: 15
  },
  RulerPoint: {
    fontSize: 12,
    fontFamily: 'OpenSans-Bold',
    color: '#223263',
    width: 38,
  },
  FuelValue: {
    color: '#223263',
    fontSize: 30,
    fontFamily: 'OpenSans-Bold',
    marginTop: 18
  }
})