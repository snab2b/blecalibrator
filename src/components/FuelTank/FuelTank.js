import React from 'react'
import { Image, View, Text } from 'react-native'
import styles from './fuel-tank-styles'

const FuelTank = () => {
  const fuel = 40

  return (
    <View style={styles.FuelTankContainer}>
      <Image
        style={styles.FuelTankImage}
        resizeMode='contain'
        source={require('../../assets/images/tank.png')}
      />

      <View style={styles.FuelTankJalousieContainer}>
        <View style={[styles.FuelTankJalousie, { height: `${100 - fuel}%` }]} />
      </View>

      <Image
        style={styles.FuelTankBorder}
        resizeMode='contain'
        source={require('../../assets/images/tank-border.png')}
      />

      <View style={styles.FuelRulerContainer}>
        <View style={styles.FuelRuler}>
          <Text style={styles.RulerPoint}>100%</Text>
          <Text style={styles.RulerPoint}>90%</Text>
          <Text style={styles.RulerPoint}>80%</Text>
          <Text style={styles.RulerPoint}>70%</Text>
          <Text style={styles.RulerPoint}>60%</Text>
          <Text style={styles.RulerPoint}>50%</Text>
          <Text style={styles.RulerPoint}>40%</Text>
          <Text style={styles.RulerPoint}>30%</Text>
          <Text style={styles.RulerPoint}>20%</Text>
          <Text style={styles.RulerPoint}>10%</Text>
        </View>
      </View>

      <Text style={styles.FuelValue}>{fuel} л</Text>
    </View>
  )
}

export default FuelTank