import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';

const Modal = ({show, setShow, titleText, message, cancelText, confirmText}) => {

  return (
    <AwesomeAlert
      show={show}
      showProgress={false}
      title={titleText}
      message={message}
      closeOnTouchOutside={true}
      closeOnHardwareBackPress={false}
      showCancelButton={true}
      showConfirmButton={true}
      cancelText={cancelText}
      confirmText={confirmText}
      confirmButtonColor="#DD6B55"
      onCancelPressed={() => setShow(false)}
      onConfirmPressed={() => setShow(false)}
      contentContainerStyle={{
        borderRadius: 10,
        paddingTop: 15,
        paddingRight: 20,
        paddingLeft: 20,
        paddingBottom: 30,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        textAlign: 'left',
      }}
      contentStyle={{
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        textAlign: 'left',
      }}
      titleStyle={{
        paddingVertical: 0,
        paddingHorizontal: 0,
        marginBottom: 14,
        display: 'flex',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        fontSize: 16,
        lineHeight: 16,
        color: '#2F4267',
        fontFamily: 'OpenSans-Bold',
        width: '100%',
        textAlign: 'left',
      }}
      messageStyle={{
        paddingTop: 0,
        fontSize: 14,
        color: '#2F4267',
        fontFamily: 'OpenSans-Regular',
      }}
      cancelButtonStyle={{
        borderRadius: 20,
        backgroundColor: '#EDEFF4',
        paddingTop: 8,
        paddingBottom: 8,
        paddingRight: 20,
        paddingLeft: 20,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
      cancelButtonTextStyle={{
        fontFamily: 'OpenSans-Bold',
        color: '#2F4267',
        fontSize: 14,
        lineHeight: 19,
        textTransform: 'uppercase'
      }}
      confirmButtonStyle={{
        borderRadius: 20,
        backgroundColor: '#FFF',
        borderColor: '#C5CBD9',
        borderWidth: 1,
        paddingTop: 8,
        paddingBottom: 8,
        paddingRight: 20,
        paddingLeft: 20,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
      confirmButtonTextStyle={{
        fontFamily: 'OpenSans-Bold',
        color: '#2F4267',
        fontSize: 14,
        lineHeight: 19,
        textTransform: 'uppercase'
      }}
    />
  );
};

export default Modal;
