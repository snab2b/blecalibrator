import React from 'react';
import {Text, View, TouchableOpacity} from "react-native";
import switchStyles from './switch-styles'
import mainStyles from '../../assets/styles/main-styles'

const Switch = () => {
  return (
    <TouchableOpacity style={[switchStyles.SwitchContainer]}>
      <Text style={[switchStyles.SwitchTitle]}> Bluetooth и местоположение </Text>
      <View style={[mainStyles.Switcher]}>
        <View style={[mainStyles.SwitcherBall]}/>
      </View>
    </TouchableOpacity>
  );
};

export default Switch;
