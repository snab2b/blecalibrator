'use strict';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  SwitchContainer: {
    paddingTop: 20,
    paddingBottom: 20,
    marginLeft: 10,
    marginRight: 10,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row'

  },
  SwitchBorder: {
    borderColor: '#EDEFF4',
    borderBottomWidth: 10
  },
  SwitchTitle:{
    fontWeight: 'bold',
    color: '#2F4267',
    fontSize: 15
  }
})

