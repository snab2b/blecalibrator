import React from 'react';
import {View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import lineStyles from './line-styles';
import {useNavigation, useRoute} from '@react-navigation/native';

const CalibratorLineStep = () => {
  const route = useRoute();
  const rName = route.name

  const color = [
    ['#49B869', '#34B883', '#1EB9A0', '#0DB9B4', '#03B9C1', '#00B9C5'],
    ['#EDEFF4', '#EDEFF4'],
  ];


  return (
    <View style={lineStyles.LineArea}>
      <LinearGradient
        colors={color[0]}
        start={{x: 0, y: 0.5}}
        end={{x: 1, y: 0.5}}
        style={lineStyles.Line}>
      </LinearGradient>

      <LinearGradient
        colors={color[rName === "step2" || rName === "step3" ? 0 : 1]}
        start={{x: 0, y: 0.5}}
        end={{x: 1, y: 0.5}}
        style={lineStyles.Line}>
      </LinearGradient>

      <LinearGradient
        colors={color[rName === "step3"  ? 0 : 1]}
        start={{x: 0, y: 0.5}}
        end={{x: 1, y: 0.5}}
        style={lineStyles.Line}>
      </LinearGradient>

    </View>
  );
};

export default CalibratorLineStep;
