'use strict';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  LineArea:{
    backgroundColor: '#fff',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 16
  },
  Line:{
    width: 64,
    height: 6,
    borderRadius: 5,
    marginRight: 7,
    marginLeft: 7
  }
})
