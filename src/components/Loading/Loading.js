import React, { useEffect, useState, useContext } from 'react'
import { Button, View, Text, SafeAreaView, Image } from 'react-native'
import loadingStyles from "./loading-styles"

const Loading = ({typeImage, width = 21, height = 21, margins = true}) => {

  let icon;
  if (typeImage === 'small') {
    icon = require('../../assets/images/loading-21.gif');
  }

  return (
    <SafeAreaView style={loadingStyles.LoadingContainer}>
      {(typeImage === 'small' || typeImage === 'dark') &&
      <Image
        style={{
          width,
          height,
          marginLeft: 'auto',
          marginRight: 'auto'
        }}
        source={icon}
      />
      }
    </SafeAreaView>
  );
};

export default Loading;
