'use strict';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  LoadingContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  }
})
