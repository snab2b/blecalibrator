import React, { useContext } from 'react'
import { View, Text } from 'react-native'
import tableStyles from './table-styles'
import { MainContext } from '../../context/main'

const Table = () => {
  const MAIN_CONTEXT = useContext(MainContext)

  let tableSteps = <View style={[tableStyles.Data]}>
    <Text style={[tableStyles.DataText]}>1</Text>
  </View>

  let tableLabel = <View style={[tableStyles.Data]}>
    <Text style={[tableStyles.DataText]}>10</Text>
  </View>

  let tableValue = <View style={[tableStyles.Data]}>
    <Text style={[tableStyles.DataText]}>10%</Text>
  </View>

  if (MAIN_CONTEXT.calibrationTable) {
    tableSteps = MAIN_CONTEXT.calibrationTable.map((_, index) => {
      return (
        <View style={[tableStyles.Data]} key={`s${index}`}>
          <Text style={[tableStyles.DataText]}>{index + 1}</Text>
        </View>
      )
    })

    tableLabel = MAIN_CONTEXT.calibrationTable.map((item, index) => {
      return (
        <View style={[tableStyles.Data]} key={`l${index}`}>
          <Text style={[tableStyles.DataText]}>{item}</Text>
        </View>
      )
    })

    tableValue = MAIN_CONTEXT.calibrationTable.map((item, index) => {
      return (
        <View style={[tableStyles.Data]} key={`v${index}`}>
          <Text style={[tableStyles.DataText]}>{item}%</Text>
        </View>
      )
    })
  }

  return (
    <View style={[tableStyles.Table]}>
      <View style={[tableStyles.Col, { marginLeft: 0, Width: 60 }]}>
        <View style={[tableStyles.Header]}>
          <Text style={[tableStyles.HeaderText]}>
            Шаг
          </Text>
        </View>

        {tableSteps}
      </View>

      <View style={[tableStyles.Col, { flex: 1 }]}>
        <View style={[tableStyles.Header]}>
          <Text style={[tableStyles.HeaderText]}>
            Объем (л)
          </Text>
        </View>

        {tableLabel}
      </View>
      <View style={[tableStyles.Col, { marginRight: 0, borderRightWidth: 0, flex: 1 }]}>
        <View style={[tableStyles.Header]}>
          <Text style={[tableStyles.HeaderText]}>
            Показания
            датчика
          </Text>
        </View>
        
        {tableValue}
      </View>
    </View>
  )
}

export default Table
