import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  Table: {
    marginTop: 35,
    marginBottom: 30,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    // borderBottomWidth: 1,
    // borderColor: '#C5CBD9',
  },
  Col: {
    display: 'flex',
    flexDirection: 'column',
    // paddingRight: 2,
    // marginRight: 2,
    borderColor: '#C5CBD9',
    borderRightWidth: 1,
  },
  Header: {
    marginRight: 2,
    marginLeft: 2,
    backgroundColor: '#EDEFF4',
    borderRadius: 5,
    minHeight: 64,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  HeaderText: {
    color: '#2F4267',
    fontFamily: 'OpenSans-Bold',
    fontSize: 15,
    lineHeight: 16,
    textAlign: 'center'
  },
  Data: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    marginTop: 2,
    marginBottom: 2,
    borderColor: '#C5CBD9',
    borderBottomWidth: 1,
  },
  DataText: {
    color: '#2F4267',
    fontFamily: 'OpenSans-Regular',
    fontSize: 15,
    lineHeight: 21,
  },
  LineV: {

    // borderLeftWidth: 1,
    // borderStyle: 'dashed',
    // borderRadius: 1,
    // borderColor: '#C5CBD9'
  },
});

