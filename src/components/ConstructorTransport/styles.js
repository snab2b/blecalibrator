import styled from 'styled-components'

export const NoSignalArea = styled.View`
  align-items: center
`
export const NoSignalAreaText = styled.Text`
  margin: 41px 0 0 0
  font-weight: bold
  font-size: 18px
  line-height: 25px
  color: #2F4267
  font-family: 'OpenSans-Bold'
`
export const AbsoluteIndex = styled.Text`
  position: absolute
  top: 3px
  left: -7px
  background: #f00
  color: #fff
`
export const ListTransport = styled.View`
  align-items: center
  justify-content: flex-end
  flex-wrap: nowrap
  flex-direction: row
  padding: 0 30px 0 0 
  height: 100%
  transform: rotate(-90deg)
`
