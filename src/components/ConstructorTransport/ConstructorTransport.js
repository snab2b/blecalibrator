import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { Image, StyleSheet, Text, View, TouchableOpacity, ScrollView, Dimensions } from 'react-native'
import Loading from '../Loading/Loading'
import { ConnectionsContext } from '../../screens/bluetooth/context/ConnectionsContext'

import carsData from '../../screens/constructor/utils/carsData'
import trailerData from '../../screens/constructor/utils/trailerData'
import { ListTransport } from "./styles";

const window = Dimensions.get("window");
const screen = Dimensions.get("screen")

export default ({ drawData, source = 'p', isAdding = false }) => {

  const CC = useContext(ConnectionsContext)
  const navigation = useNavigation()
  const [isLoading, setLoading] = useState(true)
  const [noSignal, setNoSignal] = useState(true)

  const [dimensions, setDimensions] = useState({ window, screen });
  const onChange = ({ window, screen }) => {
    setDimensions({ window, screen });
  }

  useFocusEffect(
    useCallback(() => {
      Dimensions.addEventListener("change", onChange);
      setLoading(false)
      return () => {
        setLoading(true)
      };
    }, [isLoading]),
  )

  const stylesImg = (model) => {
    const ebanyiVRot = () => {
      if (model.type === 'B' && (model.id === 13 || model.id === 14)) return 'contain'

      return 'cover'
    }

    return {
      width: model.sizeModelColor[0],
      height: model.sizeModelColor[1],
      resizeMode: ebanyiVRot(),  //contain //cover
    }
  }

  const aWS = (position, bg) => {
    return {
      backgroundColor: bg,
      position: 'absolute',
      zIndex: 2,
      borderRadius: 5,
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      top: position.top,
      right: position.right,
      width: position.width,
      height: position.height,
      transform: [{ scaleX: 1 }, { scaleY: 1 }],
    }
  }

  const drawWheels = (type) => {
    if (type === 'car') return drawCar()
    if (type === 'trailer') return drawTrailer()
  }

  const drawCar = () => {
    const head = drawData.constructor.head
    const car = carsData.data.filter(item => item.id === head)[0]

    const SORTED_WHEELS = []

    let count = 0

    car.positionAxisModelColor.forEach(pa => {
      pa[2].forEach(wheel => {
        count++

        SORTED_WHEELS.push({
          empty: true,
          position: {
            id: count - 1,
            right: pa[0],
            top: wheel[0],
            width: pa[1][0],
            height: pa[1][1],
          },
          owner: 0
        })
      })
    })

    drawData.tires.forEach((tire, i) => {
      if (tire.owner === 0 && !!tire.index && tire.index - 1 < SORTED_WHEELS.length) {

        SORTED_WHEELS[tire.index - 1] = {
          ...tire,
          limits: drawData.limits[i],
          empty: false,
          position: SORTED_WHEELS[tire.index - 1].position
        }
      }
    })

    return drawSorted(SORTED_WHEELS)
  }

  const drawTrailer = () => {
    const current = drawData.constructor.current
    if (current) {
      trailer = trailerData.data.filter(item => item.id === drawData.constructor.trailers[current - 1])
      if (trailer.length === 0) return
      trailer = trailer[0]
    } else return

    const SORTED_WHEELS = []

    let count = 0

    trailer.positionAxisModelColor.forEach(pa => {
      pa[2].forEach(wheel => {
        count++

        SORTED_WHEELS.push({
          empty: true, position: {
            id: count - 1,
            right: pa[0],
            top: wheel[0],
            width: pa[1][0],
            height: pa[1][1],
          },
          owner: current
        })
      })
    })

    drawData.tires.forEach((tire, i) => {
      if (tire.owner === current && tire.index !== 0 && tire.index - 1 < SORTED_WHEELS.length) {
        SORTED_WHEELS[tire.index - 1] = {
          ...tire,
          limits: drawData.limits[i],
          empty: false,
          position: SORTED_WHEELS[tire.index - 1].position
        }
      }
    })

    return drawSorted(SORTED_WHEELS)
  }

  const drawSorted = (SORTED_WHEELS) => {
    return SORTED_WHEELS.map((SW, index) => {
      let valueText = ''
      let value = (!SW.empty) ? SW[source] : ''

      if (source === 'p' && value !== null && value !== '') {
        valueText = parseFloat(value.toFixed(2))
      } else {
        valueText = value
      }

      const bg = getBg(SW, value, valueText)

      return (
        <View
          key={index}
          style={{ ...aWS(SW.position, bg) }}>
          {/* <AbsoluteIndex style={{ transform: [{ rotate: '90deg' }] }}>
            {(SW.index !== undefined && SW.index > 0) && `${SW.index},${SW.pos + 1},${SW.abs + 1}`}
          </AbsoluteIndex> */}
          <Text style={styles.count}>
            {valueText}
          </Text>

        </View>
      )
    })
  }

  const getBg = (SW, value) => {
    let bg = '#444'

    if (SW.empty) return bg

    switch (source) {
      case 'p':
        if (SW.isErrorP || SW.isResettedErrorP) {
          bg = '#E12C2C'
        } else {
          bg = '#49B869'
        }

        break

      case 't':
        if (SW.isErrorT || SW.isResettedErrorT) {
          bg = '#E12C2C'
        } else {
          bg = '#49B869'
        }

        break

      case 'b':
        if (value > 66) {
          bg = '#49B869'
        } else if (value > 33) {
          bg = '#FFDB4D'
        } else {
          bg = '#E12C2C'
        }

        if (SW.isErrorB || SW.isResettedErrorB) {
          bg = '#E12C2C'
        }

        break

      default:
        break
    }

    return bg
  }

  let modelCar = null
  let modelTrailer = null
  let sizeFrame = 0

  if (!!CC.lastData) {
    if (drawData.constructor.head) {
      const head = drawData.constructor.head
      modelCar = carsData.data.filter(item => item.id === head)[0]
      sizeFrame += modelCar.sizeModelColor[0]
    }

    const current = drawData.constructor.current
    if (current) {
      modelTrailer = trailerData.data.filter(item => item.id === drawData.constructor.trailers[current - 1])
      if (modelTrailer.length > 0) {
        modelTrailer = modelTrailer[0]
        sizeFrame += modelTrailer.sizeModelColor[0]
      } else {
        modelTrailer = null
      }
    }else{
      sizeFrame += 50
    }
  }

  return (

    <View>

      {modelCar !== null &&
        <ScrollView style={{
          height: dimensions.window.height - 180
        }}>
          <View style={{
            justifyContent: 'flex-start',
            alignItems: 'center',
            height: sizeFrame
          }}>
            <ListTransport
              style={{
              width: sizeFrame
              }}>
              {modelTrailer !== null
                &&
                <View style={styles.itemTrailer}>
                  {drawWheels('trailer')}
                  <Image style={stylesImg(modelTrailer)} source={modelTrailer.imgModelNoWheels} />
                </View>
              }

              <View style={styles.item}>
                {drawWheels('car')}
                <Image style={stylesImg(modelCar)} source={modelCar.imgModelNoWheels} />
              </View>
            </ListTransport>
          </View>
        </ScrollView>
      }
    </View>

  )
}

const styles = StyleSheet.create({
  item: {
    position: 'relative',
  },
  itemTrailer: {
    position: 'relative',
    marginRight: -10,
  },
  count: {
    transform: [{ rotate: '90deg' }],
    fontSize: 12,
    lineHeight: 13,
    fontWeight: '700',
    color: '#fff',
  },
  container: {
    alignItems: 'center',
    transform: [{ rotate: '0deg' }],
    justifyContent: 'center',
    flex: 1,
  },
  bottom: {
    width: 280,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    minHeight: 46,
    borderRadius: 10,
    backgroundColor: '#00B9C5',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomText: {
    color: '#fff',
    fontWeight: '700',
    fontSize: 18,
    lineHeight: 25
  }
})
