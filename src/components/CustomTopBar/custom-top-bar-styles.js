'use strict';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  customTabBarArea: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative'
  },
  customTabBarAreaText: {
    color: '#fff',
    fontSize: 24,
    lineHeight: 33,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 0,
    marginBottom: 0
  },
  buttonBack: {
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    left: 0
  }
})

