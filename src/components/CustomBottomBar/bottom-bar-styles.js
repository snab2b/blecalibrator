'use strict';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  TabArea: {
    height: 54,
    backgroundColor: '#fff',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 20},
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 10,
  },
  TabItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative'
  },
  TabItemText: {
    marginTop: 4,
    fontSize: 8,
    lineHeight: 11,
    color: '#2F4267',
    textTransform: "uppercase"
  }
})
