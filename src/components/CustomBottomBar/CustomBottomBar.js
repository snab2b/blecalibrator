import React from 'react';
import {Button, View, Text, SafeAreaView, TouchableOpacity, Modal} from 'react-native';
import bottomBarStyles from './bottom-bar-styles';
import Condition from '../../assets/images/condition.svg';
import ConditionActive from '../../assets/images/condition-active.svg';
import Fuel from '../../assets/images/fuel.svg';
import FuelActive from '../../assets/images/fuel-active.svg';
import Contact from '../../assets/images/contact.svg';
import ContactActive from '../../assets/images/contact-active.svg';
import { useNavigation } from '@react-navigation/native'

const CustomBottomBar = ({state}) => {

  const navigation = useNavigation();

  const goTo = async (screenName) => {
    navigation.navigate(screenName);
  };

  return (
    <View style={bottomBarStyles.TabArea}>
      <TouchableOpacity
        onPress={() => goTo('State')}
        style={bottomBarStyles.TabItem}>
        {state.index === 0
          ?
          <>
            <ConditionActive width="14" height="16"/>
            <Text style={[bottomBarStyles.TabItemText, {color: '#0DB9B4'}]}>
              Состояние
            </Text>
          </>
          :
          <>
            <Condition width="14" height="16"/>
            <Text style={bottomBarStyles.TabItemText}>
              Состояние
            </Text>
          </>
        }
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => goTo('Fuel')}
        style={bottomBarStyles.TabItem}>
        {state.index === 1
          ?
          <>
            <FuelActive width="14" height="15"/>
            <Text style={[bottomBarStyles.TabItemText, {color: '#0DB9B4'}]}>
              Уровень топлива
            </Text>
          </>
          :
          <>
            <Fuel width="14" height="15"/>
            <Text style={bottomBarStyles.TabItemText}>
              Уровень топлива
            </Text>
          </>
        }
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => goTo('Contact')}
        style={bottomBarStyles.TabItem}>
        {state.index === 2
          ?
          <>
            <ContactActive width="14" height="15"/>
            <Text style={[bottomBarStyles.TabItemText, {color: '#0DB9B4'}]}>
              Контакты
            </Text>
          </>
          :
          <>
            <Contact width="15" height="15"/>
            <Text style={bottomBarStyles.TabItemText}>
              Контакты
            </Text>
          </>
        }
      </TouchableOpacity>
    </View>
  );
};

export default CustomBottomBar;
