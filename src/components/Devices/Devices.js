import React, {useEffect, useState, useContext} from 'react';
import {Button, View, Text, SafeAreaView} from 'react-native';
import DeviceList from './DevicesList';
import Loading from '../Loading/Loading';
import devicesStyles from './devices-style';

const Devices = () => {
  const [isScanning, setIsScanning] = useState(false);
  return (
    <SafeAreaView style={devicesStyles.DevicesContainer}>
      <View style={devicesStyles.DevicesView}>
        <Text style={devicesStyles.DeviceListTitle}>
          Выберите устройство
        </Text>
        <Loading
          typeImage={'small'}
          width={21}
          height={21}
        />
      </View>

      <DeviceList/>

    </SafeAreaView>
  );
};

export default Devices;
