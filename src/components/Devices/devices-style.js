'use strict';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  DevicesContainer: {
    paddingTop: 20,
    paddingBottom: 20,
    marginLeft: 10,
    marginRight: 10,
  },
  DevicesView:{
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  DeviceListTitle: {
    color: "#2F4267",
    fontFamily: 'OpenSans-Bold',
    marginBottom: 10,
    fontSize: 16,
    textAlign: 'left',
    width: '100%'
  },
  RefreshButton:{
    backgroundColor: '#fff',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#C5CBD9',
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 10,
    paddingLeft: 10,
    marginBottom: 10
  },
  RefreshText:{
    fontSize: 13,
    textTransform: 'uppercase',
    color: '#2F4267',
    fontFamily: 'OpenSans-Bold',
  },
  DeviceListContainer:{
    marginBottom: 16
  },
  DeviceContainer:{
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 16,
    borderBottomWidth: 1,
    borderColor: '#C5CBD9'
  },
  Device:{
    color: "#2F4267",
    fontSize: 16,
    fontFamily: 'OpenSans-Regular',
  },
  DeviceSelected:{
    fontFamily: 'OpenSans-Bold'
  }
})
