import React, {useState} from 'react';
import {Button, View, Text, SafeAreaView, TouchableOpacity, Modal} from 'react-native';
import devicesStyles from './devices-style';
import modalStyles from "../../assets/styles/modal-styles"
import Loading from '../Loading/Loading';

const DevicesList = () => {
  const [isModal, setModal] = useState(true);
  return (
    <SafeAreaView style={devicesStyles.DeviceListContainer}>
      <TouchableOpacity style={devicesStyles.DeviceContainer}>
        <Text style={devicesStyles.Device}>
          Имя устройства
        </Text>
        <Text style={devicesStyles.DeviceSelected}>
          Имя устройства select
        </Text>
      </TouchableOpacity>
      <Modal
        animationType='fade'
        transparent={true}
        visible={isModal}>
          <SafeAreaView style={modalStyles.ModalContainer}>
            <SafeAreaView style={modalStyles.ModalContent}>
              <SafeAreaView style={modalStyles.ModalLoading}>
                <Text style={modalStyles.ModalLoadingText}>
                  Подключаемся
                </Text>
                <Loading
                  typeImage={'small'}
                  width={21}
                  height={21}
                />
              </SafeAreaView>
            </SafeAreaView>
          </SafeAreaView>
      </Modal>
    </SafeAreaView>
  );
};

export default DevicesList;
