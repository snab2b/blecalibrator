import {Dimensions} from "react-native"


export const sizeScreen =()=>{
  return {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  }
}
