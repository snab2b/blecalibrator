'use strict';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  ModalContainer: {
    padding: 32,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  ModalContent:{
    padding: 32,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2},
    shadowOpacity: 0.1,
    elevation: 16,
    borderRadius: 10
  },
  ModalLoadingText:{
    color: '#2F4267',
    fontFamily: 'OpenSans-Regular',
    fontSize: 16,
    marginRight: 16
  },
  ModalLoading:{
    width: 200,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  CancelButton:{
    width: '100%',
    height: 46,
    backgroundColor: '#EDEFF4',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    paddingLeft: 20,
    paddingRight: 20
  },
  CancelButtonText:{
    fontFamily: 'OpenSans-Bold',
    fontSize: 15,
    lineHeight: 25,
    color: '#2F4267'
  }
})
