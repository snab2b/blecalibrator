import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  TankContainer: {
    backgroundColor: '#fff',
    paddingTop: 17,
    paddingLeft: 17,
    paddingBottom: 50,
    paddingRight: 17
  },
})