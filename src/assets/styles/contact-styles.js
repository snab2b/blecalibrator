'use strict';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  ContainerContact:{
    backgroundColor: '#fff',
    paddingTop: 17,
    paddingLeft: 17,
    paddingBottom: 20,
    paddingRight: 17,
    height: '100%'
  },
  ContactText: {
    fontSize: 14,
    fontFamily: 'OpenSans-Regular',
    lineHeight: 25,
    color: '#2F4267',
    marginBottom: 15,
    textTransform: 'uppercase'
  },
  ContactPhone:{
    fontFamily: 'OpenSans-Regular',
    fontSize: 21,
    lineHeight: 26,
    color: "#2F4267",
    marginBottom: 11
  },
  Bold:{
    fontFamily: 'OpenSans-Bold'
  },
  ContactEmail:{
    color: '#2F4267',
    textDecorationLine: 'underline',
    fontFamily: 'OpenSans-Regular',
    fontSize: 16,
    lineHeight: 25
  },
  ContactAdress:{
    marginTop: 37
  },
  Text:{
    fontFamily: 'OpenSans-Regular',
    fontSize: 14,
    lineHeight: 25,
    color: '#2F4267',
    textTransform: 'uppercase'
  },
  AdressTitle:{
    fontSize: 14,
    lineHeight: 24,
    color: '#2F4267',
    marginTop: 5
  }
})
