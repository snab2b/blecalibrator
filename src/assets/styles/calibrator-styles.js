'use strict';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  StepText:{
    fontSize: 16,
    fontFamily: 'OpenSans-Regular',
    lineHeight: 22,
    marginBottom: 12,
    textAlign: 'center',
    color: '#2F4267'
  },
  StepBold:{
    color: '#2F4267',
    fontFamily: 'OpenSans-Bold',
  },
  BtnStyle:{
    width: 290,
    height: 46,
    marginTop: 20,
    borderRadius: 25
  },
  LineProgress:{
    position: 'relative',
    width: '100%',
    height: 4,
    borderRadius: 10,
    backgroundColor: '#EDEFF4',
    marginBottom: 38,
    marginTop: 8
  },
  Progress:{
    borderRadius: 10,
    height: 4
  },
  StepTextP:{
    fontSize: 16,
    fontFamily: 'OpenSans-Regular',
    lineHeight: 22,
    color: '#2F4267'
  },
  PercText:{
    top: 5,
    position: 'absolute',
    fontSize: 15,
    fontFamily: 'OpenSans-Regular',
    lineHeight: 20,
    color: '#2F4267'
  },

  FinishText:{
    fontSize: 20,
    fontFamily: 'OpenSans-Regular',
    lineHeight: 30,
    textAlign: 'center',
    color: '#2F4267'
  },
  FinishTextBold:{
    fontSize: 20,
    fontFamily: 'OpenSans-Bold',
    lineHeight: 30,
    textAlign: 'center',
    color: '#2F4267'
  }
})
