'use strict';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    // paddingTop: 15,
    width: '100%',
    height: '100%',
    position: 'relative',
    flex: 1,
    backgroundColor: '#fff',
    zIndex: 1,
    paddingBottom: 20,
  },
  justifyCenter: {
    justifyContent: 'center',
  },
  alignCenter: {
    alignItems: 'center',
  },
  btnAquamarine: {
    backgroundColor: '#00B9C5',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    borderRadius: 30,
    paddingRight: 25,
    paddingLeft: 25,
    paddingBottom: 10,
    paddingTop: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnAquamarineText: {
    fontFamily: 'OpenSans-Bold',
    fontSize: 18,
    lineHeight: 25,
    color: '#fff',
  },
  Switcher: {
    width: 50,
    height: 25,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#97A1B3',
    position: 'relative',
  },
  SwitcherBall: {
    backgroundColor: '#C5CBD9',
    width: 16,
    height: 16,
    borderRadius: 8,
    position: 'absolute',
    left: 5,
    top: 3,
  },
  Input: {
    height: 26,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 10,
    paddingLeft: 10,
    borderRadius: 3,
    borderColor: '#C5CBD9',
    borderWidth: 1,
    fontSize: 13,
    lineHeight: 18,
    fontFamily: 'OpenSans-Regular',
    color: '#2F4267',
  },
  BtnGray: {
    borderRadius: 20,
    backgroundColor: '#EDEFF4',
    fontSize: 16,
    lineHeight: 22,
    paddingTop: 12,
    paddingBottom: 12,
    paddingRight: 10,
    paddingLeft: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  BtnGrayText:{
    fontFamily: 'OpenSans-Bold',
    color: '#2F4267',
    fontSize: 16,
    lineHeight: 22,
  },
  BtnWhite: {
    borderRadius: 20,
    backgroundColor: '#FFF',
    borderColor: '#C5CBD9',
    borderWidth: 1,
    fontSize: 16,
    lineHeight: 22,
    paddingTop: 12,
    paddingBottom: 12,
    paddingRight: 20,
    paddingLeft: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  BtnWhiteText:{
    fontFamily: 'OpenSans-Bold',
    color: '#2F4267',
    fontSize: 16,
    lineHeight: 22,
  }
});
