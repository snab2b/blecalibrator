import React from 'react';
import {ScrollView, View} from 'react-native';
import FuelTank from '../components/FuelTank/FuelTank';
import MainStyles from '../assets/styles/main-styles';
import CustomTabBar from '../components/CustomTopBar/CustomTopBar';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

const Fuel = () => {

  const page = () => {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <View style={MainStyles.container}>
          <FuelTank/>
        </View>
      </ScrollView>
    );
  };

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="B"
        component={page}
        options={{
          header: props => <CustomTabBar {...props} label={['Уровень', 'топлива (л)']} typeImage="menu"/>,
          headerShown: true,
        }}
      />
    </Stack.Navigator>
  );
};

export default Fuel;
