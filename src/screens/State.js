import React, {useContext} from 'react';
import {Button, View, Text, SafeAreaView, TouchableOpacity, Modal, ScrollView} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import mainStyles from '../assets/styles/main-styles';
import { ConnectionsContext } from './bluetooth/context/ConnectionsContext'
import CustomTabBar from '../components/CustomTopBar/CustomTopBar';
import ConstructorTransport from '../components/ConstructorTransport/ConstructorTransport';

const Stack = createStackNavigator();

const State = () => {

  const page = () => {
    const navigation = useNavigation();
    const CC = useContext(ConnectionsContext);
    console.log('CC', CC.lastData );
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <View style={[mainStyles.container, mainStyles.justifyCenter, mainStyles.alignCenter]}>
          {/*<TouchableOpacity*/}
          {/*  style={[mainStyles.btnAquamarine, {width: 280, height: 46}]}*/}
          {/*  onPress={e => navigation.navigate('CalibratorStep1')}*/}
          {/*>*/}
          {/*  <Text style={[mainStyles.btnAquamarineText]}>*/}
          {/*    Начать калибровку*/}
          {/*  </Text>*/}
          {/*</TouchableOpacity>*/}

          <ConstructorTransport
            drawData={CC.lastData}
          />

        </View>
      </ScrollView>
    );
  };

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="page"
        component={page}
        options={{
          header: props => <CustomTabBar {...props} label={['Калибровка', 'ДУТ']} typeImage="menu"/>,
          headerShown: true,
        }}
      />
    </Stack.Navigator>
  );

};

export default State;
