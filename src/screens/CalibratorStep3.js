import React, { useState, useEffect, useContext } from 'react'
import { View, Text, TouchableOpacity, ScrollView } from 'react-native'
import mainStyles from '../assets/styles/main-styles'
import calibratorStyles from '../assets/styles/calibrator-styles'
import CalibratorLineStep from '../components/CalibratorLineStep/CalibratorLineStep'
import { useNavigation } from '@react-navigation/native'
import LinearGradient from 'react-native-linear-gradient'
import Loading from '../components/Loading/Loading'
import CustomTabBar from '../components/CustomTopBar/CustomTopBar'
import { createStackNavigator } from '@react-navigation/stack'
import { MainContext } from '../context/main'

const Stack = createStackNavigator()

const CalibratorStep2 = () => {
  const MAIN_CONTEXT = useContext(MainContext)
  const navigation = useNavigation()

  useEffect(() => {
    // console.log('step', MAIN_CONTEXT.step)
    const p = Math.round(MAIN_CONTEXT.step / (MAIN_CONTEXT.calibrationTable.length - 1) * 100)
    setProgress(p)
  }, [MAIN_CONTEXT.step])

  const [progress, setProgress] = useState(0)

  const posPerc = () => {
    let pos = progress

    if (progress <= 10) {
      pos = progress
    }

    if (progress > 10 && progress < 90) {
      pos = progress - 5
    }

    if (progress >= 90) {
      pos = progress - 12
    }

    return pos
  }

  const next = async () => {
    if (MAIN_CONTEXT.step < (MAIN_CONTEXT.calibrationTable.length - 1)) {
      MAIN_CONTEXT.setStep(MAIN_CONTEXT.step + 1)
    } else {
      navigation.navigate('FinishCalibration')
    }
  }

  const step3 = () => {
    return (
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={mainStyles.container}>
          <CalibratorLineStep />
          <Text style={[calibratorStyles.StepText, { textTransform: 'uppercase' }]}>
            3 шаг
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <Text style={[calibratorStyles.StepText, calibratorStyles.StepBold]}>
              Залейте топливо до отметки {MAIN_CONTEXT.calibrationTable[MAIN_CONTEXT.step]} л
            </Text>
          </View>
          <View style={[calibratorStyles.LineProgress]}>
            <View>
              <LinearGradient
                colors={['#49B869', '#34B883', '#1EB9A0', '#0DB9B4', '#03B9C1', '#00B9C5']}
                start={{ x: 0, y: 0.5 }}
                end={{ x: 1, y: 0.5 }}
                style={[calibratorStyles.Progress, { width: progress + '%' }]}>
              </LinearGradient>
              <Text style={[calibratorStyles.PercText, { left: posPerc() + '%' }]}>
                {progress} %
              </Text>
            </View>

          </View>
          <View>
            <Text style={[calibratorStyles.StepTextP, calibratorStyles.StepBold]}>
              Дождитесь спокойствия топлива.
            </Text>
            <Text style={[calibratorStyles.StepTextP]}>
              Кнопка “Далее” активируется после совершенного действия.
            </Text>

            <View style={{ marginTop: 20 }}>
              <Loading
                typeImage={'small'}
                width={21}
                height={21}
              />
            </View>

          </View>
          <View style={mainStyles.alignCenter}>
            <TouchableOpacity
              onPress={() => next()}
              style={[mainStyles.BtnGray, calibratorStyles.BtnStyle]}>
              <Text style={[mainStyles.BtnGrayText, { textTransform: 'uppercase' }]}>
                далее
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={e => navigation.navigate('CalibratorStep1')}
              style={[mainStyles.BtnWhite, calibratorStyles.BtnStyle]}>
              <Text style={[mainStyles.BtnWhiteText]}>
                Начать калибровку заново
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    )
  }


  return (
    <Stack.Navigator>
      <Stack.Screen
        name="step3"
        component={step3}
        options={{
          header: props => <CustomTabBar {...props} label={['Калибровка', 'ДУТ']} typeImage="menu" />,
          headerShown: true,
        }}
      />
    </Stack.Navigator>
  )
}

export default CalibratorStep2
