import React from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {useNavigation} from '@react-navigation/native';
import mainStyles from '../assets/styles/main-styles';
import calibratorStyles from '../assets/styles/calibrator-styles';
import OkIcon from '../assets/images/ok.svg';

const FinishCalibration = () => {

  const navigation = useNavigation();

  return (
    <ScrollView contentContainerStyle={{flexGrow: 1}}>
      <LinearGradient
        style={[mainStyles.container, mainStyles.alignCenter, mainStyles.justifyCenter]}
        colors={['#F0F5FF', '#FFFFFF']}
        start={{x: 0, y: 0.5}}
        end={{x: 1, y: 0.5}}>

        <OkIcon width="72" height="72" style={{marginBottom: 25}}/>

        <Text style={[calibratorStyles.FinishTextBold]}>
          Калибровка
        </Text>

        <Text style={[calibratorStyles.FinishText]}>
          успешно завершена!
        </Text>

        <TouchableOpacity
          onPress={e => navigation.navigate('TableCalibration')}
          style={[mainStyles.btnAquamarine, {marginTop: 50, width: 102}]}>
          <Text style={[mainStyles.btnAquamarineText]}>
            Ок
          </Text>
        </TouchableOpacity>

      </LinearGradient>
    </ScrollView>
  );

};

export default FinishCalibration;
