import React, { useState, useContext } from 'react'
import { View, Text, TextInput, TouchableOpacity, ScrollView } from 'react-native'
import mainStyles from '../assets/styles/main-styles'
import calibratorStyles from '../assets/styles/calibrator-styles'
import CalibratorLineStep from '../components/CalibratorLineStep/CalibratorLineStep'
import { useNavigation } from '@react-navigation/native'
import CustomTabBar from '../components/CustomTopBar/CustomTopBar'
import { createStackNavigator } from '@react-navigation/stack'
import Loading from '../components/Loading/Loading'
import { MainContext } from '../context/main'

const Stack = createStackNavigator()

const CalibratorStep1 = () => {
  const MAIN_CONTEXT = useContext(MainContext)
  const navigation = useNavigation()

  const step1 = () => {
    const [size, setSize] = useState('50')
    const [isLoading, setIsLoading] = useState(false)

    const calculate = () => {
      setIsLoading(true)

      const intSize = parseInt(size, 10)

      const table = Array.from({ length: 20 }, () => null)
      table[0] = 0

      if (intSize <= 60) {
        for (let i = 1; i < 15; i++) {
          if(i < 5) {
            table[i] = intSize/40 * i
          } if (i >= 5 && i < 13) {
            table[i] = intSize / 10 * (i - 3)
          } else if(i >= 13){
            table[i] = intSize / 20 * (i + 6)
          }
        }
      } else {
        for (let i = 1; i < 20; i++) {
          if (i < 6) {
            table[i] = intSize / 30 * i
          } if (i >= 6 && i < 16) {
            table[i] = intSize / 15 * (i - 3)
          } else if (i >= 16) {
            table[i] = intSize / 20 * (i + 1)
          }
        }
      }

      const finalTable = []

      table.forEach(item => {
        if (item !== null) finalTable.push(parseFloat(item.toFixed(2)))
      })

      console.log('finalTable', finalTable)
      setTimeout(next, 0, finalTable)
    }

    const next = async (table) => {
      await MAIN_CONTEXT.setStep(1)
      await MAIN_CONTEXT.setCalibrationTable(table)

      // setIsLoading(false)
      navigation.navigate('CalibratorStep2')
    }

    return (
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={mainStyles.container}>
          <CalibratorLineStep />

          <Text style={[calibratorStyles.StepText, { textTransform: 'uppercase' }]}>
            1 шаг
          </Text>

          <View style={{ flexDirection: 'row' }}>
            <Text style={[calibratorStyles.StepText, calibratorStyles.StepBold]}>Введите объем бака</Text>
            <Text style={[calibratorStyles.StepText]}>(л)</Text>
          </View>

          <TextInput
            value={size ? size : ''}
            onChangeText={value => setSize(value)}
            style={mainStyles.Input}
            keyboardType="phone-pad"
          />

          <View style={mainStyles.alignCenter}>
            {isLoading &&
              <View style={{ height: 46, marginTop: 20 }}>
                <Loading
                  typeImage={'small'}
                  width={21}
                  height={21}
                />
              </View>
            }

            {!isLoading &&
              <TouchableOpacity
                onPress={() => calculate()}
                style={[mainStyles.BtnGray, calibratorStyles.BtnStyle]}
              >
                <Text style={[mainStyles.BtnGrayText, { textTransform: 'uppercase' }]}>
                  Далее
                </Text>
              </TouchableOpacity>
            }
          </View>
        </View>
      </ScrollView>
    )
  }

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="step1"
        component={step1}
        options={{
          header: props => <CustomTabBar {...props} label={['Калибровка', 'ДУТ']} typeImage="menu" />,
          headerShown: true,
        }}
      />
    </Stack.Navigator>
  )
}

export default CalibratorStep1
