import React, {useEffect} from 'react';
import {Linking, View, Text, ScrollView} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import CustomTabBar from '../components/CustomTopBar/CustomTopBar';
import contactStyles from '../assets/styles/contact-styles';

const Stack = createStackNavigator();

export default () => {

  const Contact = () => {
    // console.log('sizeScreen.height', sizeScreen().height);
    return (
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={[contactStyles.ContainerContact]}>
          <Text style={contactStyles.ContactText}>
            ПОДДЕРЖКА ПН-ПТ С 09:00 ДО 18:00
          </Text>
          <Text style={contactStyles.ContactPhone} onPress={() => Linking.openURL(`tel:84957276700`)}>
            8 (495) <Text style={contactStyles.Bold}>727-67-00</Text>
          </Text>
          <Text style={contactStyles.ContactText}>
            ТЕХПОДДЕРЖКА:
          </Text>
          <Text style={contactStyles.ContactEmail}
                onPress={() => Linking.openURL(`mailto:support@tyrecontrol.ru`)}>
            support@<Text style={contactStyles.Bold}>tyrecontrol</Text>.ru
          </Text>
          <View style={contactStyles.ContactAdress}>
            <Text style={contactStyles.AdressTitle}>
              <Text style={contactStyles.Bold}>Контакты</Text> компании
            </Text>
            <Text style={contactStyles.Text}>ООО “ВМ ГРУПП”</Text>
            <Text style={contactStyles.Text}>123290, МОСКВА Г, ШЕЛЕПИХИНСКАЯ НАБ</Text>
            <Text style={contactStyles.Text}>ДОМ № 18</Text>
          </View>
          <View style={[contactStyles.ContactAdress]}>
            <Text style={contactStyles.AdressTitle}>
              <Text style={contactStyles.Bold}>Реквизиты</Text> компании
            </Text>
            <Text style={contactStyles.Text}>ОГРН: 1197746267135</Text>
            <Text style={contactStyles.Text}>ИНН: 7703474575</Text>
            <Text style={contactStyles.Text}>КПП: 770301001</Text>
            <Text style={contactStyles.Text}>БИК: 044525700</Text>
            <Text style={contactStyles.Text}>Р/СЧ: 40702810600000123111</Text>
            <Text style={contactStyles.Text}>К/СЧ: 30101810200000000700</Text>
            <Text style={[contactStyles.Text]}>БАНК: АО "РАЙФФАЙЗЕНБАНК"</Text>

          </View>
        </View>
      </ScrollView>
    );
  };

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="B"
        component={Contact}
        options={{
          header: props => <CustomTabBar {...props} label={['Контакты', '']} typeImage="menu"/>,
          headerShown: true,
        }}
      />
    </Stack.Navigator>
  );
}
