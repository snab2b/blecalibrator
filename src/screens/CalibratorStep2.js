import React from 'react';
import {Linking, View, Text, TextInput, TouchableOpacity, ScrollView} from 'react-native';
import mainStyles from '../assets/styles/main-styles';
import calibratorStyles from '../assets/styles/calibrator-styles';
import CalibratorLineStep from '../components/CalibratorLineStep/CalibratorLineStep';
import {useNavigation} from '@react-navigation/native';
import CustomTabBar from '../components/CustomTopBar/CustomTopBar';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

const CalibratorStep2 = () => {

  const navigation = useNavigation();

  const step2 = () => {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <View style={mainStyles.container}>
          <CalibratorLineStep/>
          <Text style={[calibratorStyles.StepText, {textTransform: 'uppercase'}]}>
            2 шаг
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={[calibratorStyles.StepText, calibratorStyles.StepBold]}>
              Слейте все топливо из бака
            </Text>
          </View>
          <View style={mainStyles.alignCenter}>
            <TouchableOpacity
              onPress={e => navigation.navigate('CalibratorStep3')}
              style={[mainStyles.BtnGray, calibratorStyles.BtnStyle]}>
              <Text style={[mainStyles.BtnGrayText, {textTransform: 'uppercase'}]}>
                далее
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={e => navigation.navigate('CalibratorStep1')}
              style={[mainStyles.BtnWhite, calibratorStyles.BtnStyle]}>
              <Text style={[mainStyles.BtnWhiteText]}>
                Начать калибровку заново
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  };

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="step2"
        component={step2}
        options={{
          header: props => <CustomTabBar {...props} label={['Калибровка', 'ДУТ']} typeImage="menu"/>,
          headerShown: true,
        }}
      />
    </Stack.Navigator>
  );
};

export default CalibratorStep2;
