import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import Switch from '../components/Switch/Switch';
import Devices from '../components/Devices/Devices';
import mainStyles from '../assets/styles/main-styles';

const Bluetooth = () => {

  return (
    <ScrollView contentContainerStyle={{flexGrow: 1}}>
      <View style={mainStyles.container}>
        <Switch/>
        <Devices/>
      </View>
    </ScrollView>
  );
};

export default Bluetooth;
