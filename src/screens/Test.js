import React, {useState} from 'react';
import {ScrollView, StyleSheet, Text, TextInput, View} from 'react-native';
import mainStyles from '../assets/styles/main-styles';

export default function Test() {
  const [size, setSize] = useState('50');
  return (
    <ScrollView contentContainerStyle={{flexGrow: 1}}>
      <View style={styles.container}>
        <Text>Test</Text>
        <TextInput
          value={size ? size : ''}
          onChangeText={value => setSize(value)}
          style={mainStyles.Input}/>
      </View>
    </ScrollView>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
