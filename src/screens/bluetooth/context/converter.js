export default {
  hexToDec(str) {
    let result = ''

    for (i = 0; i < str.length; i += 2) {
      result += parseInt(str.substr(i, 2), 16).toString()
    }

    return result
  },
  hexAsciiDec(str) {
    let result = ''

    for (i = 0; i < str.length; i += 2) {
      result += String.fromCharCode(parseInt(str.substr(i, 2), 16).toString())
    }

    return result
  },
  hex2bin(hex) {
    return ("00000000" + (parseInt(hex, 16)).toString(2)).substr(-8)
  },
  hexToPress(hexP) {
    const int = (parseInt(hexP, 16) * 5.49 - 100) / 100
    return int >= 0 ? int : 0
  },
  hexToTemp(hexT) {
    return parseFloat(parseInt(hexT, 16) - 50)
  },
  hexToBattery(hexB) {
    const int = parseInt(hexB, 16)
    return int <= 100 ? int : 100
  },
  stringToDecArray(str) {
    const array = []

    let cnt = 0
    let buf = ''

    for (let i = 0; i < str.length; i++) {
      if (cnt !== 2) {
        cnt++
        buf += str[i].toString()
      } else {
        array.push(parseInt(buf, 16))
        cnt = 1
        buf = str[i].toString()
      }
    }

    if (cnt) array.push(parseInt(buf, 16))

    return array
  },
  convertAll(hex) {
    const result = {}

    const operator = {
      value: parseInt(hex.substr(286, 2), 10),
      hex: hex.substr(286, 2),
      apn: this.hexAsciiDec(hex.substr(0, 40)),
      name: this.hexAsciiDec(hex.substr(40, 40)),
      password: this.hexAsciiDec(hex.substr(80, 40)),
      ip: this.hexAsciiDec(hex.substr(120, 40)),
      port: this.hexAsciiDec(hex.substr(160, 40)),
    }
    const phone = this.hexAsciiDec(hex.substr(240, 40))

    const v1 = parseInt(hex.substr(280, 2), 16)
    const v2 = parseInt(hex.substr(282, 2), 16)
    const version = `${v1}.${v2.length > 1 ? v2 : `0${v2}`}`

    const errorsHex = hex.substr(284, 2)

    const addingHEX = hex.substr(328, 14)

    const adding = {
      hex: addingHEX,
      isAdding: parseInt(addingHEX.substr(0, 2), 16),
      id: addingHEX.substr(2, 6),
      dataHEX: addingHEX.substr(8, 6),
      p: this.hexToPress(addingHEX.substr(8, 2)),
      t: this.hexToTemp(addingHEX.substr(10, 2)),
      b: this.hexToBattery(addingHEX.substr(12, 2)),
    }

    const constructorHEX = hex.substr(342, 14)
    const constructor = {
      hex: constructorHEX,
      current: parseInt(constructorHEX.substr(0, 2), 16),
      head: parseInt(constructorHEX.substr(2, 2), 16),
      trailers: [
        parseInt(constructorHEX.substr(4, 2), 16),
        parseInt(constructorHEX.substr(6, 2), 16),
        parseInt(constructorHEX.substr(8, 2), 16),
      ]
    }

    const tires = []
    const limits = []

    let now = null
    let count = 1
    let lastCount = 0

    const tireAbs = []

    for (i = 0; i < 46; i++) {
      const tireHEX = hex.substr((356 + i * 24), 24)

      const owner = parseInt(tireHEX.substr(18, 2), 16)
      const index = parseInt(tireHEX.substr(20, 2), 16)

      if (now === null && index > 0) {
        now = owner
      } else if (now === owner) {
        count++
      } else if (now !== owner) {
        now = owner
        lastCount += count
        count = 1
      }

      if (index > 0) tireAbs.push(i)

      const errorHex = tireHEX.substr(22, 2)
      const errorBin = this.hex2bin(errorHex)

      tires.push({
        isEmpty: index <= 0,
        hex: tireHEX,
        id: tireHEX.substr(0, 6),

        p: this.hexToPress(tireHEX.substr(6, 2)),
        t: this.hexToTemp(tireHEX.substr(8, 2)),
        b: this.hexToBattery(tireHEX.substr(10, 2)),

        owner,
        index,
        pos: (owner > 0 ? i - lastCount : i),
        abs: i,

        errorHex,
        errorBin,
        error: parseInt(tireHEX.substr(23, 1), 16) > 0,
        isErrorP: errorBin[7] === '1',
        isErrorT: errorBin[6] === '1',
        isErrorB: errorBin[5] === '1',
        isResettedErrorP: errorBin[4] === '1',
        isResettedErrorT: errorBin[3] === '1',
        isResettedErrorB: errorBin[2] === '1',
      })

      limits.push({
        index,
        pos: (owner > 0 ? i - lastCount : i),
        abs: i,

        p: {
          maxHex: tireHEX.substr(12, 2),
          minHex: tireHEX.substr(14, 2),
          max: this.hexToPress(tireHEX.substr(12, 2)),
          min: this.hexToPress(tireHEX.substr(14, 2))
        },
        t: {
          min: -999,
          maxHex: tireHEX.substr(16, 2),
          max: this.hexToTemp(tireHEX.substr(16, 2)),
        }
      })
    }

    result.tireAbs = tireAbs
    result.operator = operator
    result.phone = phone.substr(0, 12)
    result.adding = adding
    result.version = version
    result.tires = tires
    result.limits = limits
    result.constructor = constructor

    result.errors = {
      hex: errorsHex,
      sim: parseInt(errorsHex, 16) === 1 || parseInt(errorsHex, 16) === 3,
      memory: parseInt(errorsHex, 16) === 2 || parseInt(errorsHex, 16) === 3,
    }

    return result
  },
  prepareSendData(changes, lastHex, lastData) {
    let newHex = lastHex

    if (changes.phone) {
      newHex = newHex.substring(0, 240) + changes.phone + newHex.substring(280)
    }

    if (changes.ip) {
      newHex = newHex.substring(0, 120) + changes.ip + newHex.substring(160)
    }

    if (changes.port) {
      newHex = newHex.substring(0, 160) + changes.port + newHex.substring(200)
    }

    if (changes.operator) {
      newHex = newHex.substring(0, 286) + `0${changes.operator}` + newHex.substring(288)
    }

    if (changes.apnData) {
      newHex = newHex.substring(0, 0) + changes.apnData.APN + newHex.substring(40)
      newHex = newHex.substring(0, 40) + changes.apnData.NAME + newHex.substring(80)
      newHex = newHex.substring(0, 80) + changes.apnData.PASSWORD + newHex.substring(120)
    }

    if (changes.limits) {
      // console.log('limits', changes.limits)
      changes.limits.forEach(limit => {
        newHex = newHex.substring(0, 368 + limit.abs * 24) + limit.hex + newHex.substring(374 + limit.abs * 24)
        newHex = newHex.substring(0, 378 + limit.abs * 24) + '00' + newHex.substring(380 + limit.abs * 24)
      })
    }

    if (changes.constructor) {
      console.log(changes.constructor)
      // changes.constructor = {
      //   head: 25,
      //   trailers: [1, 3, 0, 0, 0],
      //   tires: [{abs: 1, index: 2, owner: 0}, ... {abs: 8, index: 1, owner: 1}]
      // }
      if (changes.constructor.head) {
        const hex = changes.constructor.head.toString(16)
        newHex = newHex.substring(0, 344) + `${hex.length > 1 ? hex : `0${hex}`}` + newHex.substring(346)

        lastData.tires.forEach((item, index) => {
          if (item.index > 0 && item.owner === 0) {
            newHex = newHex.substring(0, 356 + index * 24) + '000000000000000000000000' + newHex.substring(380 + index * 24)
          }
        })
      }

      if (changes.constructor.trailers) {
        let newTrailersHex = ''
        let lastTrailer = 0
        changes.constructor.trailers.forEach((item, index) => {
          const hex = item.toString(16)
          newTrailersHex += `${hex.length > 1 ? hex : `0${hex}`}`
          if (item > 0) {
            lastTrailer = index + 1
          }
        })

        newHex = newHex.substring(0, 346) + newTrailersHex + newHex.substring(356)

        if (lastTrailer > 0) {
          newHex = newHex.substring(0, 343) + lastTrailer + newHex.substring(344)
        }
      }

      // if (changes.constructor.tires) {
      //   changes.constructor.tires.forEach(item => {
      //     const index = item.index.toString(16)
      //     const owner = item.owner.toString(16)
      //
      //     newHex = newHex.substring(0, 374 + item.abs * 24) + `${owner.length > 1 ? owner : `0${owner}`}` + newHex.substring(376 + item.abs * 24)
      //     newHex = newHex.substring(0, 376 + item.abs * 24) + `${index.length > 1 ? index : `0${index}`}` + newHex.substring(378 + item.abs * 24)
      //   })
      // }
    }

    if (changes.trailer) {
      if (changes.trailer === 9) {
        newHex = newHex.substring(0, 343) + '0' + newHex.substring(344)
      } else {
        newHex = newHex.substring(0, 343) + changes.trailer + newHex.substring(344)
      }
    }

    return newHex
  },
  prepareResetErrors(lastHex, lastData) {
    let newHex = lastHex

    lastData.tires.forEach(item => {
      if (item.error) {
        const resetBin = `00${item.isErrorB || item.isResettedErrorB ? '1' : '0'}${item.isErrorT || item.isResettedErrorT ? '1' : '0'}${item.isErrorP || item.isResettedErrorP ? '1' : '0'}000`
        const hexReset = parseInt(resetBin, 2).toString(16)
        // console.log('errors', item.errorBin, resetBin, parseInt(resetBin, 2), )
        newHex = newHex.substring(0, 378 + item.abs * 24) + `${hexReset.length > 1 ? hexReset : `0${hexReset}`}` + newHex.substring(380 + item.abs * 24)
      }
    })

    return newHex
  },
  prepareResetTrailer(trailer, lastHex, lastData) {
    console.log('prepareResetTrailer', trailer)
    let newHex = lastHex
    let shiftIndex = 0

    const newTrailers = []

    console.log('oldTrailers', lastData.constructor.trailers)
    lastData.constructor.trailers.forEach((item, index) => {
      if (index !== (trailer - 1)) {
        newTrailers.push(item)
      } else {
        shiftIndex = index + 1
      }
    })
    newTrailers.push(0)

    console.log('newTrailers', newTrailers)
    console.log('shiftIndex', shiftIndex)

    let newTrailersHex = ''

    newTrailers.forEach(item => {
      const hex = item.toString(16)
      newTrailersHex += `${hex.length > 1 ? hex : `0${hex}`}`
    })

    console.log('newTrailersHex', newTrailersHex)

    newHex = newHex.substring(0, 346) + newTrailersHex + newHex.substring(352) // заменили массив трейлеров

    if (lastData.constructor.current > 0) {
      if (lastData.constructor.current === trailer || lastData.constructor.current > trailer) {
        newHex = newHex.substring(0, 343) + '0' + newHex.substring(344)
      }
    }

    // if (lastData.constructor.current === trailer) {
    //   if (newTrailers[0] > 0) {
    //     newHex = newHex.substring(0, 343) + '1' + newHex.substring(344)
    //   } else {
    //     newHex = newHex.substring(0, 343) + '0' + newHex.substring(344)
    //   }
    // }else{

    // }

    // newHex = newHex.substring(0, 343) + '0' + newHex.substring(344)

    lastData.tires.forEach(item => {
      if (item.index > 0 && item.owner === trailer) {
        newHex = newHex.substring(0, 356 + item.abs * 24) + '000000000000000000000000' + newHex.substring(380 + item.abs * 24)
      }
    })

    lastData.tires.forEach(item => {
      if (item.index > 0 && item.owner > shiftIndex) {
        newHex = newHex.substring(0, 375 + item.abs * 24) + (item.owner - 1) + newHex.substring(376 + item.abs * 24)
      }
    })

    return newHex
  },
  prepareStartAdding(lastHex) {
    let newHex = lastHex

    newHex = newHex.substring(0, 328) + '01000000000000' + newHex.substring(342)
    // newHex = newHex.substring(0, 328) + '01' + newHex.substring(330)

    return newHex
  },
  prepareCancelAdding(tire, lastHex, lastData) {
    let newHex = lastHex

    newHex = newHex.substring(0, 328) + '00000000000000' + newHex.substring(342)
    // newHex = newHex.substring(0, 328) + '00' + newHex.substring(330)

    let isAdded = false

    if (tire) {
      console.log('tire', tire)
      console.log('tire.data', tire.data)

      const id = tire.id
      const data = tire.data
      const index = `${tire.index.toString(16).length > 1 ? tire.index.toString(16) : `0${tire.index.toString(16)}`}`

      lastData.tires.forEach(item => {
        if (!isAdded && (item.owner === 0 && item.index === 0 || item.owner === tire.owner && item.index === tire.index)) {
          newHex = newHex.substring(0, 356 + item.abs * 24) + `${id}${data}7D148C0${tire.owner}${index}00` + newHex.substring(380 + item.abs * 24)
          isAdded = true
        }
      })
    }

    return newHex
  },
  convertHexaDecimalToAscii(asc) {
    const maxlen = 40
    asc = asc.trim()
    let res = ''
    let u = unescape(encodeURIComponent(asc))
    res += u.split('').map(function (c) { return c.charCodeAt(0).toString(16) }).join('')

    return res.split('').concat(Array(maxlen).fill(0)).slice(0, 40).join('')
  },
  pressToHex(dec) {
    return Math.round((dec * 100 + 100) / 5.49).toString(16)
  },
  tempToHex(dec) {
    return Math.round(dec + 50).toString(16)
  }
}
