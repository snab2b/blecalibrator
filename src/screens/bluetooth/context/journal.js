import moment from 'moment/min/moment-with-locales'

export default {
  checkForFailure(data) {
    let result = false
    // console.log('checkForFailure', data)
    if (data.limits && data.tires) {

      data.tires.forEach((item, index) => {
        if (item.p > 0) {
          if (!result && (item.p < data.limits[index].p.min || item.p > data.limits[index].p.max)) result = true
          if (!result && item.t > data.limits[index].t.max) result = true
        }
      })
    }

    return result
  },
  checkForHexFailure(data) {
    let result = false

    if (data.tires) {
      data.tires.forEach(item => {
        if (item.isErrorP && !item.isResettedErrorP) {
          result = true
          return
        }
        if (item.isErrorT && !item.isResettedErrorT) {
          result = true
          return
        }
        if (item.isErrorB && !item.isResettedErrorB) {
          result = true
          return
        }
      })
    }

    return result
  },
  chechForFailureAndCompare(lastData, storedData = null) {
    let result = false
    let hasError = false
    let isSame = true

    if (lastData.tires) {
      lastData.tires.forEach((item, index) => {
        if (item.isErrorP && !item.isResettedErrorP) {
          hasError = true

          if (storedData && storedData.tires && storedData.tires[index]) {
            if (!storedData.tires[index].isErrorP || storedData.tires[index].isResettedErrorP) {
              isSame = false
            }
          } else {
            isSame = false
          }
        }
        if (item.isErrorT && !item.isResettedErrorT) {
          hasError = true

          if (storedData && storedData.tires && storedData.tires[index]) {
            if (!storedData.tires[index].isErrorT || storedData.tires[index].isResettedErrorT) {
              isSame = false
            }
          } else {
            isSame = false
          }
        }
        if (item.isErrorB && !item.isResettedErrorB) {
          hasError = true

          if (storedData && storedData.tires && storedData.tires[index]) {
            if (!storedData.tires[index].isErrorB || storedData.tires[index].isResettedErrorB) {
              isSame = false
            }
          } else {
            isSame = false
          }
        }
      })
    }

    if(hasError && isSame) {
      const diff = moment().diff(moment(storedData.datetime, 'YYYY-MM-DD HH:mm'), 'minutes')

      if(diff > 10) result = true
    }else if(hasError && !isSame) {
      result = true
    }

    return result
  }
}