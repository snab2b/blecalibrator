import React, { createContext, useState, useEffect, useContext } from 'react'
import ConnectivityManager from 'react-native-connectivity-status'
import BluetoothSerial from 'react-native-bluetooth-serial-next'
import RNRestart from 'react-native-restart'
import AsyncStorage from '@react-native-community/async-storage'
import BackgroundTimer from 'react-native-background-timer'
import moment from 'moment/min/moment-with-locales'
import journal from './journal'
import fakeHex from './fake'
import converter from './converter'
import { Buffer } from 'buffer'
import emptyHex from './empty'

const LENGTH = 730

export const ConnectionsContext = createContext()
let timerRead = null
let timerConnect = null

export const ConnectionsContextProvider = ({ children }) => {
    const [btStatus, setBtStatus] = useState(false)
    const [locationStatus, setLocationStatus] = useState(false)
    const [inited, setInited] = useState(false)
    const [isLoading, setIsLoading] = useState(true)
    const [isListen, setIsListen] = useState(false)
    const [isFake, setIsFake] = useState(false)
    const [isAdding, setIsAdding] = useState(false)
    const [lastData, setLastData] = useState(null)
    const [lastHex, setLastHex] = useState(null)
    const [ignore, setIgnore] = useState(0)
    const [lastUpdate, setLastUpdate] = useState(null)
    const [isReconnecting, setIsReconnecting] = useState(false)

    useEffect(() => {
        async function inite() {
            if (!inited) {
                const lHex = await AsyncStorage.getItem(`@lastHex`)
                const lData = await AsyncStorage.getItem(`@lastData`)
                if (lHex && lData) {
                    setLastData({ ...JSON.parse(lData), offline: true })
                    setLastHex(lHex)

                    setLastUpdate(moment().format('YYYY-MM-DD HH:mm:ss'))
                } else {
                    console.log('lastHex is not found')
                }

                checkStatuses()

                ConnectivityManager.addStatusListener(({ eventType, status }) => { // LISTENER
                    switch (eventType) {
                        case 'bluetooth':
                            setBtStatus(status)
                            break
                        case 'location':
                            setLocationStatus(status)
                            break
                    }
                })
            }
        }

        inite()
    }, [])

    useEffect(() => {
        if (!inited) {
            checkStatuses()
        }
    }, [inited])

    // useEffect(() => {
    //     console.log('lastUpdate', lastUpdate)
    // }, [lastUpdate])

    const isCorrectLength = (hex) => {
        const L = LENGTH * 2

        if (hex.length === L) return true

        return false
    }

    const checkForReconnect = () => {
        let diff = 0

        if (!lastUpdate) {
            setLastUpdate(moment().format('YYYY-MM-DD HH:mm:ss'))
        } else {
            diff = moment().diff(moment(lastUpdate, 'YYYY-MM-DD HH:mm:ss'), 'seconds')
        }

        console.log('checkForReconnect diff:', diff)

        if (diff > 40) {
            // RNRestart.Restart()
            setIsListen(false)
            setIsReconnecting(true)
        }
    }

    useEffect(() => {
        if (isReconnecting) {
            reconnecting()
            timerConnect = BackgroundTimer.setInterval(async () => {
                reconnecting()
            }, 30000)
        } else {
            BackgroundTimer.clearInterval(timerConnect)
        }
    }, [isReconnecting])

    const reconnecting = async () => {
        console.log('reconnecting...')

        const savedId = await AsyncStorage.getItem('_device')
        let diff = 0

        if (!lastUpdate) {
            setLastUpdate(moment().format('YYYY-MM-DD HH:mm:ss'))
        } else {
            diff = moment().diff(moment(lastUpdate, 'YYYY-MM-DD HH:mm:ss'), 'seconds')
        }

        if (isFake) {
            // if (diff < 60) {
                console.log('Fake reconnection complete')

                function fakeReconnect() {
                    setLastUpdate(moment().format('YYYY-MM-DD HH:mm:ss'))
                    setIsReconnecting(false)
                    setIsListen(true)
                }

                BackgroundTimer.setTimeout(fakeReconnect, 2000)
            // } else RNRestart.Restart()
        } else {
            if (diff < 300 && !isAdding) {
                if (!!savedId) {
                    console.log('Connection lost, trying to reconnect')

                    await BluetoothSerial.disconnect(savedId)

                    console.log('disconnected')

                    BluetoothSerial.device(savedId).connect().then(() => {
                        console.log('reconnected OK')
                        setLastUpdate(moment().format('YYYY-MM-DD HH:mm:ss'))
                        setIsReconnecting(false)
                        setIsListen(true)
                    }).catch(e => {
                        console.log('err:::', e)
                    })
                } else {
                    console.log('device is missing')
                }
            } else RNRestart.RNRestart()
        }
    }

    useEffect(() => {
        if (isListen) {
            timerRead = BackgroundTimer.setInterval(async () => {
                if (isFake) {
                    listenFake()
                } else {
                    listenReal()
                }

                checkForReconnect()
            }, (isFake ? 15000 : 3000))
        } else {
            BackgroundTimer.clearInterval(timerRead)
        }
    }, [isListen])

    const listenReal = async () => {
        const savedId = await AsyncStorage.getItem('_device')

        if (savedId) {
            const data = await BluetoothSerial.readFromDevice(savedId)

            console.log(`recieved ${data.length} chars:::`, data.toUpperCase())

            if (!!data) {
                console.log('data LENGTH/2', data.length / 2)

                if (isCorrectLength(data) && data[318] === '6' && data[319] === '9') {
                    if (ignore === 0) {
                        const converted = hexToObject(data.toUpperCase())

                        console.log('recieved object', converted)

                        setLastData(converted)
                        setLastHex(data.toUpperCase())

                        await AsyncStorage.setItem(`@lastHex`, data.toUpperCase())
                        await AsyncStorage.setItem(`@lastData`, JSON.stringify(converted))
                    } else {
                        console.log('setIgnore', ignore)
                        setIgnore(ignore - 1)
                    }

                    setLastUpdate(moment().format('YYYY-MM-DD HH:mm:ss'))

                    setIsListen(false)
                    function delay() {
                        setIsListen(true)
                    }

                    BackgroundTimer.setTimeout(delay, 0)
                }
            } else {
                const lHex = await AsyncStorage.getItem(`@lastHex`)
                const lData = await AsyncStorage.getItem(`@lastData`)

                if (lHex && lData) {
                    setLastData({ ...JSON.parse(lData), offline: true })
                    setLastHex(lHex)
                } else {
                    console.log('lastHex is not found')
                }
            }
        }
    }

    const listenFake = async () => {
        if (true) { // имитация отсутствия сигнала (true - пакеты идут)
            // const fake = emptyHex
            const fake = fakeHex
            const data = hexToObject(fake)

            setLastData(data)
            setLastHex(fake)

            await AsyncStorage.setItem(`@lastHex`, fake)
            await AsyncStorage.setItem(`@lastData`, JSON.stringify(data))
            console.log('fakeData', data)

            setLastUpdate(moment().format('YYYY-MM-DD HH:mm:ss'))

            setIsListen(false)
            function delay() {
                setIsListen(true)
            }

            BackgroundTimer.setTimeout(delay, 0)
        } else {
            const lHex = await AsyncStorage.getItem(`@lastHex`)
            const lData = await AsyncStorage.getItem(`@lastData`)

            if (lHex && lData) {
                setLastData({ ...JSON.parse(lData), offline: true })
                setLastHex(lHex)
            } else {
                console.log('lastHex is not found')
            }
        }
    }

    const checkStatuses = () => {
        console.log(1);
        ConnectivityManager.isBluetoothEnabled().then(state => {
            setBtStatus(state)
            console.log(2);
            ConnectivityManager.areLocationServicesEnabled().then(state => {
                console.log(3);
                setLocationStatus(state)

                setInited(true)
                setIsLoading(false)
                console.log(4);
            })
        })
    }

    const hexToObject = (hex) => {
        return { ...converter.convertAll(hex), datetime: moment().format('YYYY-MM-DD HH:mm:ss') }
    }

    useEffect(() => {
        const checkToSave = async () => {
            // const NOW = moment().format('YY-MM-DD')
            // await AsyncStorage.removeItem(`@${NOW}`)
            if (lastData.offline) return

            const NOW = moment().format('YYYY-MM-DD')
            const JSONdayJournal = await AsyncStorage.getItem(`@${NOW}`)

            let storedData = null

            if (!!JSONdayJournal) {
                const parse = JSON.parse(JSONdayJournal)
                storedData = parse[parse.length - 1]
            }

            const needToSave = journal.chechForFailureAndCompare(lastData, storedData)

            if (needToSave) {
                const dayJournal = (JSONdayJournal ? JSON.parse(JSONdayJournal) : [])

                dayJournal.push(lastData)
                await AsyncStorage.setItem(`@${NOW}`, JSON.stringify(dayJournal))

                console.log(`@${NOW}`, 'Нарушение сохранено')
            }
        }

        if (lastData !== null) checkToSave()
    }, [lastData])

    const sendData = async (changes = {}) => {
        const savedId = await AsyncStorage.getItem('_device')
        if (!savedId && !isFake) {
            console.log('device is missing')
            return
        }

        const newHex = converter.prepareSendData(changes, lastHex, lastData)
        const buffer = Buffer.alloc(730, converter.stringToDecArray(newHex), 'base64')

        setIgnore(1)
        if (!isFake) await BluetoothSerial.write(buffer, savedId)

        await finalizeSend(newHex)
    }

    const resetErrors = async () => {
        console.log('resetErrors')

        const savedId = await AsyncStorage.getItem('_device')
        if (!savedId && !isFake) {
            console.log('device is missing')
            return
        }

        const newHex = converter.prepareResetErrors(lastHex, lastData)

        const buffer = Buffer.alloc(730, converter.stringToDecArray(newHex), 'base64')

        setIgnore(1)
        if (!isFake) await BluetoothSerial.write(buffer, savedId)

        await finalizeSend(newHex)
    }

    const startAdding = async (ignore) => {
        // console.log('startAdding')
        const savedId = await AsyncStorage.getItem('_device')
        if (!savedId && !isFake) {
            console.log('device is missing')
            return
        }

        const newHex = converter.prepareStartAdding(lastHex)

        const buffer = Buffer.alloc(730, converter.stringToDecArray(newHex), 'base64')

        if (ignore) setIgnore(1)
        if (!isFake) await BluetoothSerial.write(buffer, savedId)

        await finalizeSend(newHex)
    }

    const cancelAdding = async (tire = null) => {
        const savedId = await AsyncStorage.getItem('_device')
        if (!savedId && !isFake) {
            console.log('device is missing')
            return
        }

        const newHex = converter.prepareCancelAdding(tire, lastHex, lastData)

        const buffer = Buffer.alloc(730, converter.stringToDecArray(newHex), 'base64')

        setIgnore(1)
        if (!isFake) await BluetoothSerial.write(buffer, savedId)

        await finalizeSend(newHex)
    }

    const resetDevice = async () => {
        // console.log('resetDevice', emptyHex)
        const savedId = await AsyncStorage.getItem('_device')
        if (!savedId && !isFake) {
            console.log('device is missing')
            return
        }

        const newHex = emptyHex

        const buffer = Buffer.alloc(730, converter.stringToDecArray(newHex), 'base64')

        setIgnore(1)
        if (!isFake) await BluetoothSerial.write(buffer, savedId)

        await finalizeSend(newHex)
    }

    const resetTrailer = async (trailer) => {
        const savedId = await AsyncStorage.getItem('_device')
        if (!savedId && !isFake) {
            console.log('device is missing')
            return
        }

        const newHex = converter.prepareResetTrailer(trailer, lastHex, lastData)

        const buffer = Buffer.alloc(730, converter.stringToDecArray(newHex), 'base64')

        setIgnore(1)
        if (!isFake) await BluetoothSerial.write(buffer, savedId)

        await finalizeSend(newHex)
    }

    const finalizeSend = async (newHex) => {
        const converted = hexToObject(newHex)

        console.log('sended', newHex, converted)

        setLastData(converted)
        setLastHex(newHex)

        await AsyncStorage.setItem(`@lastHex`, newHex)
        await AsyncStorage.setItem(`@lastData`, JSON.stringify(converted))

        return true
    }

    return (
        <ConnectionsContext.Provider
            value={{
                inited,
                setInited,
                btStatus,
                locationStatus,
                lastData,
                setBtStatus,
                setLocationStatus,
                setIsListen,
                setIsFake,
                isLoading,
                isReconnecting,
                setIsLoading,
                sendData,
                resetErrors,
                startAdding,
                cancelAdding,
                resetTrailer,
                setIsReconnecting,
                resetDevice,
                isAdding,
                setIsAdding
            }}>
            {children}
        </ConnectionsContext.Provider>
    )
}
