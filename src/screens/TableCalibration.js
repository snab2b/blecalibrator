import React, { useState } from 'react'
import { View, Text, TouchableOpacity, ScrollView } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import CustomTabBar from '../components/CustomTopBar/CustomTopBar'
import Table from '../components/Table/Table'
import mainStyles from '../assets/styles/main-styles'
import calibratorStyles from '../assets/styles/calibrator-styles'
import Modal from '../components/Modal/Modal'

const Stack = createStackNavigator()

const TableCalibration = () => {
  const [show, setShow] = useState(false)

  const page = () => {
    return (
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View style={mainStyles.container}>
          <Modal
            show={show}
            setShow={setShow}
            titleText="Начать калибровку"
            message="Вы действительно хотите начать калибровку заново?"
            cancelText="Отменить"
            confirmText="Начать"
          />

          <Table />

          <View style={mainStyles.alignCenter}>
            <TouchableOpacity
              style={[mainStyles.btnAquamarine, calibratorStyles.BtnStyle]}>
              <Text style={[mainStyles.btnAquamarineText]}>
                Сохранить результат
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => setShow(true)}
              style={[mainStyles.BtnWhite, calibratorStyles.BtnStyle]}>
              <Text style={[mainStyles.BtnWhiteText]}>
                Начать калибровку заново
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    )
  }

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="page"
        component={page}
        options={{
          header: props => <CustomTabBar {...props} label={['Таблица', 'калибровки']} typeImage="menu" />,
          headerShown: true,
        }}
      />
    </Stack.Navigator>
  )
}

export default TableCalibration
