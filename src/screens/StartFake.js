import React, {useContext, useEffect} from 'react';
import {StyleSheet, Text, View, Image, Button, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'
import {useNavigation} from '@react-navigation/native';
import {ConnectionsContext} from './bluetooth/context/ConnectionsContext';
import LinearGradient from 'react-native-linear-gradient';
import mainStyles from '../assets/styles/main-styles';
import startStyles from '../assets/styles/start-styles';
import {sizeScreen} from '../utils/helpers';

const StartFake = () => {
  const navigation = useNavigation();

  const CC = useContext(ConnectionsContext);

  useEffect(() => {
    console.log(CC);
    const checkDevice = async () => {
      await AsyncStorage.getItem('_device')
      console.log('FAKE STARTING')

      fakeConnect()
    }
    console.log(6);
    if (CC.inited) {
      console.log(7)
      checkDevice()
    }
  }, [CC.inited])

  const fakeConnect = async () => {
    console.log('FAKE connecting...');
    navigation.navigate('CabinetTab');
    CC.setIsFake(true);
    CC.setIsListen(true);

  };


  return (
    <LinearGradient
      style={[mainStyles.container]}
      start={{x: 0, y: 0.5}}
      end={{x: 1, y: 0.5}}
      colors={['#F0F5FF', '#FFFFFF']}>
      <View
        style={[
          {marginTop: '30%', alignItems: 'center'},
        ]}>
        <Image
          style={[startStyles.imgLogo]}
          resizeMode='contain'
          source={require('../assets/images/logo.png')}
        />
        <Text style={[startStyles.title]}>Калибровка ДУТ</Text>
        <Text style={[startStyles.text]}>
          Чтобы начать работу, добавьте
          устройство
        </Text>
        <TouchableOpacity
          onPress={e => navigation.push('Bluetooth')}
          style={[mainStyles.btnAquamarine]}
          title="Найти устройство">
          <Text style={[mainStyles.btnAquamarineText]}>Найти устройство</Text>
        </TouchableOpacity>
      </View>
      <Image
        style={[startStyles.imgIndicator, {left: sizeScreen().width / 2 - 120}]}
        resizeMode='contain'
        source={require('../assets/images/indicator.png')}
      />
      <Image
        style={[startStyles.imgBackground, {height: sizeScreen().height / 2.8}]}
        resizeMode='cover'
        source={require('../assets/images/city.png')}
      />
    </LinearGradient>
  );
};
export default StartFake;
