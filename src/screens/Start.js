import React from 'react';
import {StyleSheet, Text, View, Image, Button, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import mainStyles from '../assets/styles/main-styles';
import startStyles from '../assets/styles/start-styles';
import {sizeScreen} from '../utils/helpers';

const Start = () => {
  const navigation = useNavigation();
  console.log(sizeScreen());
  console.log(mainStyles);
  return (
    <LinearGradient
      style={[mainStyles.container]}
      start={{x: 0, y: 0.5}}
      end={{x: 1, y: 0.5}}
      colors={['#F0F5FF', '#FFFFFF']}>
      <View
        style={[
          {marginTop: '30%', alignItems: 'center'},
        ]}>
        <Image
          style={[startStyles.imgLogo]}
          resizeMode='contain'
          source={require('../assets/images/logo.png')}
        />
        <Text style={[startStyles.title]}>Калибровка ДУТ</Text>
        <Text style={[startStyles.text]}>
          Чтобы начать работу, добавьте
          устройство
        </Text>
        <TouchableOpacity
          onPress={e => navigation.push('Bluetooth')}
          style={[mainStyles.btnAquamarine]}
          title="Найти устройство">
          <Text style={[mainStyles.btnAquamarineText]}>Найти устройство</Text>
        </TouchableOpacity>
      </View>
      <Image
        style={[startStyles.imgIndicator, {left: sizeScreen().width / 2 - 120}]}
        resizeMode='contain'
        source={require('../assets/images/indicator.png')}
      />
      <Image
        style={[startStyles.imgBackground, {height: sizeScreen().height / 2.8}]}
        resizeMode='cover'
        source={require('../assets/images/city.png')}
      />
    </LinearGradient>
  );
};
export default Start;
